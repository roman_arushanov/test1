<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */

define('DB_NAME', 'blog');

/** Имя пользователя MySQL */
define('DB_USER', 'cpt-local');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'admin');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         'T^F^O&Bk]l/y5Lvi`,vFQSMb$mpmxaf@zT~ YFU^D}BYN}[uG^?E11furZjY7Hib');
define('SECURE_AUTH_KEY',  '>I6GJtA5V?7GdQ*9:bo}A!>PTk^x6*~(iPv5Jv}++5U~N}yw19)XXtd]yj=K([+M');
define('LOGGED_IN_KEY',    'NTzW1[,lLHRdWp4kjC)>WIl<o:YPN}q# Hlhe(oQD-QlC^F&c-Mqw-fmc/S%;8SG');
define('NONCE_KEY',        'TJI~*t0Th0%W=az;.S(DAH9t9}EgILmEk6$yZ?/8dVcW0v%[AYbz}+]A.gn}_F65');
define('AUTH_SALT',        'YV`BYPvj5s`9`QeBpf:7pPAY3~u(Ze&^kv~mg@;1twQ~J[^:H(MZ|g<pJ/j7e*iV');
define('SECURE_AUTH_SALT', 'ehT@#z hDG5M=wTUkG^TKV%cjg3W^.r<9j1f@ye7Ep$GhM5N|Ez7`lh|s> hD^B<');
define('LOGGED_IN_SALT',   'x3IWA7r#dE.W&: .-UPzbn%Z6:A1-ZNPno]<[b19fezqK{1GN-&)>,liPvIi=JA$');
define('NONCE_SALT',       'h?rsFvdj{&1.f`4Y~(hS^[O$VZ;D&>.z+FS`bi:&;]RB;gtJxq@dl#KfSQJ~=XEo');



/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */

$table_prefix  = 'wp_';


/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
