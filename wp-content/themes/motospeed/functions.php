<<<<<<< HEAD
<?php
/**
 * MotoSpeed functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 */

require_once( trailingslashit( get_template_directory() ) . 'inc/customize-pro/class-customize.php' );

if ( ! function_exists( 'motospeed_setup' ) ) :
	/**
	 * MotoSpeed setup.
	 *
	 * Set up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support post thumbnails.
	 *
	 */
	function motospeed_setup() {

		/*
		 * Make theme available for translation.
		 *
		 * Translations can be filed in the /languages/ directory
		 *
		 * If you're building a theme based on MotoSpeed, use a find and replace
		 * to change 'motospeed' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'motospeed', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );



		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, and column width.
	 	 */
		add_editor_style( array( 'assets/css/editor-style.css', 
								 get_template_directory_uri() . '/assets/css/font-awesome.css',
								 motospeed_fonts_url()
						  ) );

		/*
		 * Set Custom Background
		 */				 
		add_theme_support( 'custom-background', array ('default-color'  => '#ffffff') );

		// Set the default content width.
		$GLOBALS['content_width'] = 900;

		// This theme uses wp_nav_menu() in header menu
		register_nav_menus( array(
			'primary'   => __( 'Primary Menu', 'motospeed' ),
			'footer'    => __( 'Footer Menu', 'motospeed' ),
		) );

		$defaults = array(
	        'flex-height' => false,
	        'flex-width'  => false,
	        'header-text' => array( 'site-title', 'site-description' ),
	    );
	    add_theme_support( 'custom-logo', $defaults );
	}
endif; // motospeed_setup
add_action( 'after_setup_theme', 'motospeed_setup' );

if ( ! function_exists( 'motospeed_fonts_url' ) ) :
	/**
	 *	Load google font url used in the MotoSpeed theme
	 */
	function motospeed_fonts_url() {

	    $fonts_url = '';
	 
	    /* Translators: If there are characters in your language that are not
	    * supported by Overlock, translate this to 'off'. Do not translate
	    * into your own language.
	    */
	    $questrial = _x( 'on', 'Overlock font: on or off', 'motospeed' );

	    if ( 'off' !== $questrial ) {
	        $font_families = array();
	 
	        $font_families[] = 'Overlock';
	 
	        $query_args = array(
	            'family' => urlencode( implode( '|', $font_families ) ),
	        );
	 
	        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	    }
	 
	    return $fonts_url;
	}
endif; // motospeed_fonts_url

if ( ! function_exists( 'motospeed_load_scripts' ) ) :
	/**
	 * the main function to load scripts in the MotoSpeed theme
	 * if you add a new load of script, style, etc. you can use that function
	 * instead of adding a new wp_enqueue_scripts action for it.
	 */
	function motospeed_load_scripts() {

		// load main stylesheet.
		wp_enqueue_style( 'font-awesome',
			get_template_directory_uri() . '/assets/css/font-awesome.css', array( ) );

		wp_enqueue_style( 'animate-css',
			get_template_directory_uri() . '/assets/css/animate.css', array( ) );

		wp_enqueue_style( 'motospeed-style', get_stylesheet_uri(), array() );
		
		wp_enqueue_style( 'motospeed-fonts', motospeed_fonts_url(), array(), null );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_enqueue_script( 'viewportchecker',
			get_template_directory_uri() . '/assets/js/viewportchecker.js',
			array( 'jquery' ) );
		
		// Load Utilities JS Script
		wp_enqueue_script( 'motospeed-utilities',
			get_template_directory_uri() . '/assets/js/utilities.js',
			array( 'jquery', 'viewportchecker' ) );

		$data = array(
	        'loading_effect' => ( get_theme_mod('motospeed_animations_display', 1) == 1 ),
	    );
	    wp_localize_script('motospeed-utilities', 'motospeed_options', $data);
	}
endif; // motospeed_load_scripts
add_action( 'wp_enqueue_scripts', 'motospeed_load_scripts' );

if ( ! function_exists( 'motospeed_widgets_init' ) ) :
	/**
	 *	widgets-init action handler. Used to register widgets and register widget areas
	 */
	function motospeed_widgets_init() {
		
		// Register Sidebar Widget.
		register_sidebar( array (
							'name'	 		 =>	 __( 'Sidebar Widget Area', 'motospeed'),
							'id'		 	 =>	 'sidebar-widget-area',
							'description'	 =>  __( 'The sidebar widget area', 'motospeed'),
							'before_widget'	 =>  '',
							'after_widget'	 =>  '',
							'before_title'	 =>  '<div class="sidebar-before-title"></div><h3 class="sidebar-title">',
							'after_title'	 =>  '</h3><div class="sidebar-after-title"></div>',
						) );

		/**
		 * Add Homepage Columns Widget areas
		 */
		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #1', 'motospeed' ),
								'id' 			 =>  'homepage-column-1-widget-area',
								'description'	 =>  __( 'The Homepage Column #1 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );
							
		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #2', 'motospeed' ),
								'id' 			 =>  'homepage-column-2-widget-area',
								'description'	 =>  __( 'The Homepage Column #2 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );

		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #3', 'motospeed' ),
								'id' 			 =>  'homepage-column-3-widget-area',
								'description'	 =>  __( 'The Homepage Column #3 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );

		// Register Footer Column #1
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #1', 'motospeed' ),
								'id' 			 =>  'footer-column-1-widget-area',
								'description'	 =>  __( 'The Footer Column #1 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
		
		// Register Footer Column #2
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #2', 'motospeed' ),
								'id' 			 =>  'footer-column-2-widget-area',
								'description'	 =>  __( 'The Footer Column #2 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
		
		// Register Footer Column #3
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #3', 'motospeed' ),
								'id' 			 =>  'footer-column-3-widget-area',
								'description'	 =>  __( 'The Footer Column #3 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
	}
endif; // motospeed_widgets_init
add_action( 'widgets_init', 'motospeed_widgets_init' );

if ( ! function_exists( 'motospeed_show_copyright_text' ) ) :
	/**
	 *	Displays the copyright text.
	 */
	function motospeed_show_copyright_text() {

		$footerText = esc_html( get_theme_mod('motospeed_footer_copyright', null) );

		if ( !empty( $footerText ) ) {

			echo esc_html( $footerText ) . ' | ';		
		}
	}
endif; // motospeed_show_copyright_text


if ( ! function_exists( 'motospeed_display_slider' ) ) :
	/**
	 * Displays the Slider
	 */
	function motospeed_display_slider() {

		$allImages = array();

		for ( $i = 1; $i <= 4; ++$i ) {

			$defaultSlideImage = get_template_directory_uri().'/images/slider/' . $i .'.jpg';
			$slideImage = get_theme_mod( 'motospeed_slide'.$i.'_image', $defaultSlideImage );

			if ( $slideImage != '' ) {

				$allImages[] = $slideImage;
			}
		}

		$allImagesCount = count($allImages);

		// skip displaying of slider if there are NO images
		if ($allImagesCount == 0) {

			return;

		} else if ($allImagesCount == 1) {

			$allImages[] = $allImages[0];
			$allImages[] = $allImages[0];
			$allImages[] = $allImages[0];

		} else if ($allImagesCount == 2) {

			$allImages[] = $allImages[0];
			$allImages[] = $allImages[1];

		} else if ($allImagesCount == 3) {

			$allImages[] = $allImages[0];
		}
	?>
		<!-- slider start -->
		<div class="rm_wrapper">
			<div id="rm_container" class="rm_container">
				<ul>
					<li data-images="rm_container_1" data-rotation="-15">
						<img src="<?php echo esc_url( $allImages[ 0 ] ); ?>" />
					</li>
					<li data-images="rm_container_2" data-rotation="-5">
						<img src="<?php echo esc_url( $allImages[ 1 ] ); ?>" />
					</li>
					<li data-images="rm_container_3" data-rotation="5">
						<img src="<?php echo esc_url( $allImages[ 2 ] ); ?>" />
					</li>			
					<li data-images="rm_container_4" data-rotation="15">
						<img src="<?php echo esc_url( $allImages[ 3 ] ); ?>" />
					</li>
				</ul>
				<div id="rm_mask_left" class="rm_mask_left">
				</div>
				<div id="rm_mask_right" class="rm_mask_right">
				</div>
				<div id="rm_corner_left" class="rm_corner_left">
				</div>
				<div id="rm_corner_right" class="rm_corner_right">
				</div>
				<div style="display:none;">
					<div id="rm_container_1">
						<?php
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_2">
						<?php
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_3">
						<?php
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_4">
						<?php
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
						?>
					</div>
				</div>
			</div>
			<div class="rm_nav">
				<a id="rm_next" href="#" class="rm_next"></a>
				<a id="rm_prev" href="#" class="rm_prev"></a>
			</div>
		</div>
		<!-- slider end -->
	<?php
	}
endif; // motospeed_display_slider

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

=======
<?php
/**
 * MotoSpeed functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 */

require_once( trailingslashit( get_template_directory() ) . 'inc/customize-pro/class-customize.php' );

if ( ! function_exists( 'motospeed_setup' ) ) :
	/**
	 * MotoSpeed setup.
	 *
	 * Set up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support post thumbnails.
	 *
	 */
	function motospeed_setup() {

		/*
		 * Make theme available for translation.
		 *
		 * Translations can be filed in the /languages/ directory
		 *
		 * If you're building a theme based on MotoSpeed, use a find and replace
		 * to change 'motospeed' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'motospeed', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 */
		add_theme_support( 'post-thumbnails' );



		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * This theme styles the visual editor to resemble the theme style,
		 * specifically font, colors, and column width.
	 	 */
		add_editor_style( array( 'assets/css/editor-style.css', 
								 get_template_directory_uri() . '/assets/css/font-awesome.css',
								 motospeed_fonts_url()
						  ) );

		/*
		 * Set Custom Background
		 */				 
		add_theme_support( 'custom-background', array ('default-color'  => '#ffffff') );

		// Set the default content width.
		$GLOBALS['content_width'] = 900;

		// This theme uses wp_nav_menu() in header menu
		register_nav_menus( array(
			'primary'   => __( 'Primary Menu', 'motospeed' ),
			'footer'    => __( 'Footer Menu', 'motospeed' ),
		) );

		$defaults = array(
	        'flex-height' => false,
	        'flex-width'  => false,
	        'header-text' => array( 'site-title', 'site-description' ),
	    );
	    add_theme_support( 'custom-logo', $defaults );
	}
endif; // motospeed_setup
add_action( 'after_setup_theme', 'motospeed_setup' );

if ( ! function_exists( 'motospeed_fonts_url' ) ) :
	/**
	 *	Load google font url used in the MotoSpeed theme
	 */
	function motospeed_fonts_url() {

	    $fonts_url = '';
	 
	    /* Translators: If there are characters in your language that are not
	    * supported by Overlock, translate this to 'off'. Do not translate
	    * into your own language.
	    */
	    $questrial = _x( 'on', 'Overlock font: on or off', 'motospeed' );

	    if ( 'off' !== $questrial ) {
	        $font_families = array();
	 
	        $font_families[] = 'Overlock';
	 
	        $query_args = array(
	            'family' => urlencode( implode( '|', $font_families ) ),
	        );
	 
	        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	    }
	 
	    return $fonts_url;
	}
endif; // motospeed_fonts_url

if ( ! function_exists( 'motospeed_load_scripts' ) ) :
	/**
	 * the main function to load scripts in the MotoSpeed theme
	 * if you add a new load of script, style, etc. you can use that function
	 * instead of adding a new wp_enqueue_scripts action for it.
	 */
	function motospeed_load_scripts() {

		// load main stylesheet.
		wp_enqueue_style( 'font-awesome',
			get_template_directory_uri() . '/assets/css/font-awesome.css', array( ) );

		wp_enqueue_style( 'animate-css',
			get_template_directory_uri() . '/assets/css/animate.css', array( ) );

		wp_enqueue_style( 'motospeed-style', get_stylesheet_uri(), array() );
		
		wp_enqueue_style( 'motospeed-fonts', motospeed_fonts_url(), array(), null );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		wp_enqueue_script( 'viewportchecker',
			get_template_directory_uri() . '/assets/js/viewportchecker.js',
			array( 'jquery' ) );
		
		// Load Utilities JS Script
		wp_enqueue_script( 'motospeed-utilities',
			get_template_directory_uri() . '/assets/js/utilities.js',
			array( 'jquery', 'viewportchecker' ) );

		$data = array(
	        'loading_effect' => ( get_theme_mod('motospeed_animations_display', 1) == 1 ),
	    );
	    wp_localize_script('motospeed-utilities', 'motospeed_options', $data);
	}
endif; // motospeed_load_scripts
add_action( 'wp_enqueue_scripts', 'motospeed_load_scripts' );

if ( ! function_exists( 'motospeed_widgets_init' ) ) :
	/**
	 *	widgets-init action handler. Used to register widgets and register widget areas
	 */
	function motospeed_widgets_init() {
		
		// Register Sidebar Widget.
		register_sidebar( array (
							'name'	 		 =>	 __( 'Sidebar Widget Area', 'motospeed'),
							'id'		 	 =>	 'sidebar-widget-area',
							'description'	 =>  __( 'The sidebar widget area', 'motospeed'),
							'before_widget'	 =>  '',
							'after_widget'	 =>  '',
							'before_title'	 =>  '<div class="sidebar-before-title"></div><h3 class="sidebar-title">',
							'after_title'	 =>  '</h3><div class="sidebar-after-title"></div>',
						) );

		/**
		 * Add Homepage Columns Widget areas
		 */
		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #1', 'motospeed' ),
								'id' 			 =>  'homepage-column-1-widget-area',
								'description'	 =>  __( 'The Homepage Column #1 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );
							
		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #2', 'motospeed' ),
								'id' 			 =>  'homepage-column-2-widget-area',
								'description'	 =>  __( 'The Homepage Column #2 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );

		register_sidebar( array (
								'name'			 =>  __( 'Homepage Column #3', 'motospeed' ),
								'id' 			 =>  'homepage-column-3-widget-area',
								'description'	 =>  __( 'The Homepage Column #3 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="sidebar-title">',
								'after_title'	 =>  '</h2><div class="sidebar-after-title"></div>',
							) );

		// Register Footer Column #1
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #1', 'motospeed' ),
								'id' 			 =>  'footer-column-1-widget-area',
								'description'	 =>  __( 'The Footer Column #1 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
		
		// Register Footer Column #2
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #2', 'motospeed' ),
								'id' 			 =>  'footer-column-2-widget-area',
								'description'	 =>  __( 'The Footer Column #2 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
		
		// Register Footer Column #3
		register_sidebar( array (
								'name'			 =>  __( 'Footer Column #3', 'motospeed' ),
								'id' 			 =>  'footer-column-3-widget-area',
								'description'	 =>  __( 'The Footer Column #3 widget area', 'motospeed' ),
								'before_widget'  =>  '',
								'after_widget'	 =>  '',
								'before_title'	 =>  '<h2 class="footer-title">',
								'after_title'	 =>  '</h2><div class="footer-after-title"></div>',
							) );
	}
endif; // motospeed_widgets_init
add_action( 'widgets_init', 'motospeed_widgets_init' );

if ( ! function_exists( 'motospeed_show_copyright_text' ) ) :
	/**
	 *	Displays the copyright text.
	 */
	function motospeed_show_copyright_text() {

		$footerText = esc_html( get_theme_mod('motospeed_footer_copyright', null) );

		if ( !empty( $footerText ) ) {

			echo esc_html( $footerText ) . ' | ';		
		}
	}
endif; // motospeed_show_copyright_text


if ( ! function_exists( 'motospeed_display_slider' ) ) :
	/**
	 * Displays the Slider
	 */
	function motospeed_display_slider() {

		$allImages = array();

		for ( $i = 1; $i <= 4; ++$i ) {

			$defaultSlideImage = get_template_directory_uri().'/images/slider/' . $i .'.jpg';
			$slideImage = get_theme_mod( 'motospeed_slide'.$i.'_image', $defaultSlideImage );

			if ( $slideImage != '' ) {

				$allImages[] = $slideImage;
			}
		}

		$allImagesCount = count($allImages);

		// skip displaying of slider if there are NO images
		if ($allImagesCount == 0) {

			return;

		} else if ($allImagesCount == 1) {

			$allImages[] = $allImages[0];
			$allImages[] = $allImages[0];
			$allImages[] = $allImages[0];

		} else if ($allImagesCount == 2) {

			$allImages[] = $allImages[0];
			$allImages[] = $allImages[1];

		} else if ($allImagesCount == 3) {

			$allImages[] = $allImages[0];
		}
	?>
		<!-- slider start -->
		<div class="rm_wrapper">
			<div id="rm_container" class="rm_container">
				<ul>
					<li data-images="rm_container_1" data-rotation="-15">
						<img src="<?php echo esc_url( $allImages[ 0 ] ); ?>" />
					</li>
					<li data-images="rm_container_2" data-rotation="-5">
						<img src="<?php echo esc_url( $allImages[ 1 ] ); ?>" />
					</li>
					<li data-images="rm_container_3" data-rotation="5">
						<img src="<?php echo esc_url( $allImages[ 2 ] ); ?>" />
					</li>			
					<li data-images="rm_container_4" data-rotation="15">
						<img src="<?php echo esc_url( $allImages[ 3 ] ); ?>" />
					</li>
				</ul>
				<div id="rm_mask_left" class="rm_mask_left">
				</div>
				<div id="rm_mask_right" class="rm_mask_right">
				</div>
				<div id="rm_corner_left" class="rm_corner_left">
				</div>
				<div id="rm_corner_right" class="rm_corner_right">
				</div>
				<div style="display:none;">
					<div id="rm_container_1">
						<?php
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_2">
						<?php
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_3">
						<?php
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
						?>
					</div>
					<div id="rm_container_4">
						<?php
							echo '<img src="' . esc_url( $allImages[ 3 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 0 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 1 ] ) . '" />';
							echo '<img src="' . esc_url( $allImages[ 2 ] ) . '" />';
						?>
					</div>
				</div>
			</div>
			<div class="rm_nav">
				<a id="rm_next" href="#" class="rm_next"></a>
				<a id="rm_prev" href="#" class="rm_prev"></a>
			</div>
		</div>
		<!-- slider end -->
	<?php
	}
endif; // motospeed_display_slider

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

>>>>>>> b9cb38dd5380a6a37d92ca254d63d8e74592569c
