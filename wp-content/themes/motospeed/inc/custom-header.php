<<<<<<< HEAD
<?php
/**
 * Custom header implementation
 *
 */

if ( ! function_exists( 'motospeed_custom_header_setup' ) ) :
  /**
   * Set up the WordPress core custom header feature.
   *
   */
  function motospeed_custom_header_setup() {

  	add_theme_support( 'custom-header', array (
                         'default-image'          => '',
                         'flex-height'            => true,
                         'flex-width'             => true,
                         'uploads'                => true,
                         'width'                  => 900,
                         'height'                 => 100,
                         'default-text-color'     => '#434343',
                         'wp-head-callback'       => 'motospeed_header_style',
                      ) );
  }
endif; // motospeed_custom_header_setup
add_action( 'after_setup_theme', 'motospeed_custom_header_setup' );

if ( ! function_exists( 'motospeed_header_style' ) ) :

  /**
   * Styles the header image and text displayed on the blog.
   */
  function motospeed_header_style() {

  	$header_text_color = get_header_textcolor();

      if ( ! has_header_image()
          && ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color
               || 'blank' === $header_text_color ) ) {

          return;
      }

      $headerImage = get_header_image();
  ?>
      <style id="motospeed-custom-header-styles" type="text/css">

          <?php if ( has_header_image() ) : ?>

                  #header-main-fixed {background-image: url("<?php echo esc_url( $headerImage ); ?>");}

          <?php endif; ?>

          <?php if ( get_theme_support( 'custom-header', 'default-text-color' ) !== $header_text_color
                      && 'blank' !== $header_text_color ) : ?>

                  #header-main-fixed, #header-main-fixed h1.entry-title {color: #<?php echo sanitize_hex_color_no_hash( $header_text_color ); ?>;}

          <?php endif; ?>
      </style>
  <?php
  }
endif; // End of motospeed_header_style.

=======
<?php
/**
 * Custom header implementation
 *
 */

if ( ! function_exists( 'motospeed_custom_header_setup' ) ) :
  /**
   * Set up the WordPress core custom header feature.
   *
   */
  function motospeed_custom_header_setup() {

  	add_theme_support( 'custom-header', array (
                         'default-image'          => '',
                         'flex-height'            => true,
                         'flex-width'             => true,
                         'uploads'                => true,
                         'width'                  => 900,
                         'height'                 => 100,
                         'default-text-color'     => '#434343',
                         'wp-head-callback'       => 'motospeed_header_style',
                      ) );
  }
endif; // motospeed_custom_header_setup
add_action( 'after_setup_theme', 'motospeed_custom_header_setup' );

if ( ! function_exists( 'motospeed_header_style' ) ) :

  /**
   * Styles the header image and text displayed on the blog.
   */
  function motospeed_header_style() {

  	$header_text_color = get_header_textcolor();

      if ( ! has_header_image()
          && ( get_theme_support( 'custom-header', 'default-text-color' ) === $header_text_color
               || 'blank' === $header_text_color ) ) {

          return;
      }

      $headerImage = get_header_image();
  ?>
      <style id="motospeed-custom-header-styles" type="text/css">

          <?php if ( has_header_image() ) : ?>

                  #header-main-fixed {background-image: url("<?php echo esc_url( $headerImage ); ?>");}

          <?php endif; ?>

          <?php if ( get_theme_support( 'custom-header', 'default-text-color' ) !== $header_text_color
                      && 'blank' !== $header_text_color ) : ?>

                  #header-main-fixed, #header-main-fixed h1.entry-title {color: #<?php echo sanitize_hex_color_no_hash( $header_text_color ); ?>;}

          <?php endif; ?>
      </style>
  <?php
  }
endif; // End of motospeed_header_style.

>>>>>>> b9cb38dd5380a6a37d92ca254d63d8e74592569c
