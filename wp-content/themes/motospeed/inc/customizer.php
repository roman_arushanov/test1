<<<<<<< HEAD
<?php

if ( ! function_exists( 'motospeed_sanitize_checkbox' ) ) :
	/**
	 * Sanitization callback for 'checkbox' type controls. This callback sanitizes `$checked`
	 * as a boolean value, either TRUE or FALSE.
	 *
	 * @param bool $checked Whether the checkbox is checked.
	 * @return bool Whether the checkbox is checked.
	 */
	function motospeed_sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}
endif; // motospeed_sanitize_checkbox

if ( ! function_exists( 'motospeed_customize_register' ) ) :
	/**
	 * Add postMessage support for site title and description for the Theme Customizer.
	 *
	 */
	function motospeed_customize_register( $wp_customize ) {

		/**
	     * Add Animations Section
	     */
	    $wp_customize->add_section(
	        'motospeed_animations_display',
	        array(
	            'title'       => __( 'Animations', 'motospeed' ),
	            'capability'  => 'edit_theme_options',
	        )
	    );

	    // Add display Animations option
	    $wp_customize->add_setting(
	            'motospeed_animations_display',
	            array(
	                    'default'           => 1,
	                    'sanitize_callback' => 'motospeed_sanitize_checkbox',
	            )
	    );

	    $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
	                        'motospeed_animations_display',
	                            array(
	                                'label'          => __( 'Enable Animations', 'motospeed' ),
	                                'section'        => 'motospeed_animations_display',
	                                'settings'       => 'motospeed_animations_display',
	                                'type'           => 'checkbox',
	                            )
	                        )
	    );

		/**
		 * Add Footer Section
		 */
		$wp_customize->add_section(
			'motospeed_footer_section',
			array(
				'title'       => __( 'Footer', 'motospeed' ),
				'capability'  => 'edit_theme_options',
			)
		);
		
		// Add Footer Copyright Text
		$wp_customize->add_setting(
			'motospeed_footer_copyright',
			array(
			    'default'           => '',
			    'sanitize_callback' => 'sanitize_text_field',
			)
		);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'motospeed_footer_copyright',
	        array(
	            'label'          => __( 'Copyright Text', 'motospeed' ),
	            'section'        => 'motospeed_footer_section',
	            'settings'       => 'motospeed_footer_copyright',
	            'type'           => 'text',
	            )
	        )
		);

		/**
		 * Add Slider Section
		 */
		$wp_customize->add_section(
			'motospeed_slider_section',
			array(
				'title'       => __( 'Slider', 'motospeed' ),
				'capability'  => 'edit_theme_options',
			)
		);
		
		// Add display slider option
		$wp_customize->add_setting(
				'motospeed_slider_display',
				array(
						'default'           => 0,
						'sanitize_callback' => 'motospeed_sanitize_checkbox',
				)
		);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'motospeed_slider_display',
								array(
									'label'          => __( 'Display Homepage Slider', 'motospeed' ),
									'section'        => 'motospeed_slider_section',
									'settings'       => 'motospeed_slider_display',
									'type'           => 'checkbox',
								)
							)
		);

		for ( $i = 1; $i <= 4; ++$i ) {

			// add a slide image
			$wp_customize->add_setting( 'motospeed_slide' . $i . '_image',
				array(
					'default' => get_template_directory_uri() . '/images/slider/' . $i . '.jpg',
		    		'sanitize_callback' => 'esc_url_raw'
				)
			);

		    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'motospeed_slide' . $i . '_image',
					array(
						'label'   	 => sprintf( esc_html__( 'Slide %s Image', 'motospeed' ), $i ),
						'section' 	 => 'motospeed_slider_section',
						'settings'   => 'motospeed_slide' . $i . '_image',
					) 
				)
			);	
		}
	}
endif; // motospeed_customize_register
add_action( 'customize_register', 'motospeed_customize_register' );
=======
<?php

if ( ! function_exists( 'motospeed_sanitize_checkbox' ) ) :
	/**
	 * Sanitization callback for 'checkbox' type controls. This callback sanitizes `$checked`
	 * as a boolean value, either TRUE or FALSE.
	 *
	 * @param bool $checked Whether the checkbox is checked.
	 * @return bool Whether the checkbox is checked.
	 */
	function motospeed_sanitize_checkbox( $checked ) {
		// Boolean check.
		return ( ( isset( $checked ) && true == $checked ) ? true : false );
	}
endif; // motospeed_sanitize_checkbox

if ( ! function_exists( 'motospeed_customize_register' ) ) :
	/**
	 * Add postMessage support for site title and description for the Theme Customizer.
	 *
	 */
	function motospeed_customize_register( $wp_customize ) {

		/**
	     * Add Animations Section
	     */
	    $wp_customize->add_section(
	        'motospeed_animations_display',
	        array(
	            'title'       => __( 'Animations', 'motospeed' ),
	            'capability'  => 'edit_theme_options',
	        )
	    );

	    // Add display Animations option
	    $wp_customize->add_setting(
	            'motospeed_animations_display',
	            array(
	                    'default'           => 1,
	                    'sanitize_callback' => 'motospeed_sanitize_checkbox',
	            )
	    );

	    $wp_customize->add_control( new WP_Customize_Control( $wp_customize,
	                        'motospeed_animations_display',
	                            array(
	                                'label'          => __( 'Enable Animations', 'motospeed' ),
	                                'section'        => 'motospeed_animations_display',
	                                'settings'       => 'motospeed_animations_display',
	                                'type'           => 'checkbox',
	                            )
	                        )
	    );

		/**
		 * Add Footer Section
		 */
		$wp_customize->add_section(
			'motospeed_footer_section',
			array(
				'title'       => __( 'Footer', 'motospeed' ),
				'capability'  => 'edit_theme_options',
			)
		);
		
		// Add Footer Copyright Text
		$wp_customize->add_setting(
			'motospeed_footer_copyright',
			array(
			    'default'           => '',
			    'sanitize_callback' => 'sanitize_text_field',
			)
		);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'motospeed_footer_copyright',
	        array(
	            'label'          => __( 'Copyright Text', 'motospeed' ),
	            'section'        => 'motospeed_footer_section',
	            'settings'       => 'motospeed_footer_copyright',
	            'type'           => 'text',
	            )
	        )
		);

		/**
		 * Add Slider Section
		 */
		$wp_customize->add_section(
			'motospeed_slider_section',
			array(
				'title'       => __( 'Slider', 'motospeed' ),
				'capability'  => 'edit_theme_options',
			)
		);
		
		// Add display slider option
		$wp_customize->add_setting(
				'motospeed_slider_display',
				array(
						'default'           => 0,
						'sanitize_callback' => 'motospeed_sanitize_checkbox',
				)
		);

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'motospeed_slider_display',
								array(
									'label'          => __( 'Display Homepage Slider', 'motospeed' ),
									'section'        => 'motospeed_slider_section',
									'settings'       => 'motospeed_slider_display',
									'type'           => 'checkbox',
								)
							)
		);

		for ( $i = 1; $i <= 4; ++$i ) {

			// add a slide image
			$wp_customize->add_setting( 'motospeed_slide' . $i . '_image',
				array(
					'default' => get_template_directory_uri() . '/images/slider/' . $i . '.jpg',
		    		'sanitize_callback' => 'esc_url_raw'
				)
			);

		    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'motospeed_slide' . $i . '_image',
					array(
						'label'   	 => sprintf( esc_html__( 'Slide %s Image', 'motospeed' ), $i ),
						'section' 	 => 'motospeed_slider_section',
						'settings'   => 'motospeed_slide' . $i . '_image',
					) 
				)
			);	
		}
	}
endif; // motospeed_customize_register
add_action( 'customize_register', 'motospeed_customize_register' );
>>>>>>> b9cb38dd5380a6a37d92ca254d63d8e74592569c
