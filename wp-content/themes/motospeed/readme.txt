<<<<<<< HEAD
=== MotoSpeed ===
Contributors: customizablethemes
Tags: blog, entertainment, two-columns, right-sidebar, custom-logo, custom-background, custom-header, custom-menu, threaded-comments, translation-ready, sticky-post, theme-options, footer-widgets
Requires at least: 4.7.0
Tested up to: 4.8.2
Stable tag: 1.0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

MotoSpeed is fully Responsive Moto WordPress Theme.

== Description ==

MotoSpeed is fully Responsive Moto WordPress Theme. Features: Custom Logo, Custom Background, Footer
copyright text customizations, Widget Areas: Sidebar, 3 Footer Columns, Translation-Ready and much more.

== Frequently Asked Questions ==

= How to Display the Homepage Slider =

1. Create a Static Front Page

The slider is visualized ONLY when there is a static front page set for your website. So, the first step is:

1.1. Login to your Admin Panel
1.2. Navigate to Left Menu -> Settings -> Reading
1.3. Select 'A static page (select below)', then choose pages for 'Homepage' and 'Posts page'
1.4. Save changes

Reference: https://codex.wordpress.org/Creating_a_Static_Front_Page

2. Update Slider Settings

2.1. Login to your Admin Panel
2.2. Navigate to Left Menu -> Appearance -> Customize
2.3. Open 'Slider' Section
2.4. Check the 'Display Slider' checkbox
2.5. Set Slider Images ('Slide # Image' fields) and Text ('Slide # Content' fields) for the different Slides
2.6. Save Changes

== Changelog ==

= 1.0.9 =
* update Resources' images info 
* remove add_image_size from functions.php

= 1.0.8 =
* update readme.txt file format

= 1.0.7 =
* add instructions in readme.txt file how to configure the Homepage slider

= 1.0.6 =
* updating screenshot.png according to the new WordPress.org requirements

= 1.0.5 =
* display slider only on static homepage

= 1.0.4 =
* changing slider images

= 1.0.3 =
* add homepage slider

= 1.0.2 =
* add animations effect
* add footer menu

= 1.0.1 =
* update readme.txt file
* css bug fixes

= 1.0.0 =
* initial release

== Resources ==
* FontAwesome icon font, © Font Awesome, SIL OFL 1.1
* Customizer "Pro" theme section example, © 2016 Justin Tadlock, GNU v2.0
* assets/css/animate.css, © 2017 Daniel Eden, MIT
* assets/js/viewportchecker.js, © 2014 Dirk Groenen, MIT
* images/slider/1.jpg, © 2013 @PublicDomainPictures https://pixabay.com/en/motorcyclist-cross-man-person-218961/, CC0
* images/slider/2.jpg, © 2016 @madzArt https://pixabay.com/en/jet-ski-water-sport-water-bike-1125329/, CC0
* images/slider/3.jpg, © 2017 @PVIS https://pixabay.com/en/motocross-mx-jump-motorcycle-motor-2375660/, CC0
* images/slider/4.jpg, © 2014 @tethoa https://pixabay.com/en/motorcycle-road-motorbike-biker-552787/, CC0
* screenshot.png (slide img 1), © 2013 @PublicDomainPictures https://pixabay.com/en/motorcyclist-cross-man-person-218961/, CC0
* screenshot.png (slide img 2), © 2016 @madzArt https://pixabay.com/en/jet-ski-water-sport-water-bike-1125329/, CC0
* screenshot.png (slide img 3), © 2017 @PVIS https://pixabay.com/en/motocross-mx-jump-motorcycle-motor-2375660/, CC0
* screenshot.png (slide img 4), © 2014 @tethoa https://pixabay.com/en/motorcycle-road-motorbike-biker-552787/, CC0
=======
=== MotoSpeed ===
Contributors: customizablethemes
Tags: blog, entertainment, two-columns, right-sidebar, custom-logo, custom-background, custom-header, custom-menu, threaded-comments, translation-ready, sticky-post, theme-options, footer-widgets
Requires at least: 4.7.0
Tested up to: 4.8.2
Stable tag: 1.0.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

MotoSpeed is fully Responsive Moto WordPress Theme.

== Description ==

MotoSpeed is fully Responsive Moto WordPress Theme. Features: Custom Logo, Custom Background, Footer
copyright text customizations, Widget Areas: Sidebar, 3 Footer Columns, Translation-Ready and much more.

== Frequently Asked Questions ==

= How to Display the Homepage Slider =

1. Create a Static Front Page

The slider is visualized ONLY when there is a static front page set for your website. So, the first step is:

1.1. Login to your Admin Panel
1.2. Navigate to Left Menu -> Settings -> Reading
1.3. Select 'A static page (select below)', then choose pages for 'Homepage' and 'Posts page'
1.4. Save changes

Reference: https://codex.wordpress.org/Creating_a_Static_Front_Page

2. Update Slider Settings

2.1. Login to your Admin Panel
2.2. Navigate to Left Menu -> Appearance -> Customize
2.3. Open 'Slider' Section
2.4. Check the 'Display Slider' checkbox
2.5. Set Slider Images ('Slide # Image' fields) and Text ('Slide # Content' fields) for the different Slides
2.6. Save Changes

== Changelog ==

= 1.0.9 =
* update Resources' images info 
* remove add_image_size from functions.php

= 1.0.8 =
* update readme.txt file format

= 1.0.7 =
* add instructions in readme.txt file how to configure the Homepage slider

= 1.0.6 =
* updating screenshot.png according to the new WordPress.org requirements

= 1.0.5 =
* display slider only on static homepage

= 1.0.4 =
* changing slider images

= 1.0.3 =
* add homepage slider

= 1.0.2 =
* add animations effect
* add footer menu

= 1.0.1 =
* update readme.txt file
* css bug fixes

= 1.0.0 =
* initial release

== Resources ==
* FontAwesome icon font, © Font Awesome, SIL OFL 1.1
* Customizer "Pro" theme section example, © 2016 Justin Tadlock, GNU v2.0
* assets/css/animate.css, © 2017 Daniel Eden, MIT
* assets/js/viewportchecker.js, © 2014 Dirk Groenen, MIT
* images/slider/1.jpg, © 2013 @PublicDomainPictures https://pixabay.com/en/motorcyclist-cross-man-person-218961/, CC0
* images/slider/2.jpg, © 2016 @madzArt https://pixabay.com/en/jet-ski-water-sport-water-bike-1125329/, CC0
* images/slider/3.jpg, © 2017 @PVIS https://pixabay.com/en/motocross-mx-jump-motorcycle-motor-2375660/, CC0
* images/slider/4.jpg, © 2014 @tethoa https://pixabay.com/en/motorcycle-road-motorbike-biker-552787/, CC0
* screenshot.png (slide img 1), © 2013 @PublicDomainPictures https://pixabay.com/en/motorcyclist-cross-man-person-218961/, CC0
* screenshot.png (slide img 2), © 2016 @madzArt https://pixabay.com/en/jet-ski-water-sport-water-bike-1125329/, CC0
* screenshot.png (slide img 3), © 2017 @PVIS https://pixabay.com/en/motocross-mx-jump-motorcycle-motor-2375660/, CC0
* screenshot.png (slide img 4), © 2014 @tethoa https://pixabay.com/en/motorcycle-road-motorbike-biker-552787/, CC0
>>>>>>> b9cb38dd5380a6a37d92ca254d63d8e74592569c
