
<?php

function newtheme_post_thumbnails() {
    add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'newtheme_post_thumbnails' );
set_post_thumbnail_size( 300, 300);

function newtheme_custom_logo_setup() {
    $defaults = array(
        'height'      => 300,
        'width'       => 300,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
function newtheme_excerpt_more( $more ) {
    if ( is_admin() ) {
        return $more;
    }
    return '[.....]';
}
add_filter( 'excerpt_more', 'newtheme_excerpt_more' );

?>