<?php get_header()?>

<?php
if(have_posts()):
    while(have_posts()) : the_post();
        echo
        '<div class = "row mb-5">
            <div class = "col-sm-3">';
        if(has_post_thumbnail()):
            echo '<a href="'.the_permalink().'" title="'.the_title_attribute().'"></a>';
            the_post_thumbnail();
        elseif ( function_exists('the_custom_logo')):
            the_custom_logo();
        endif;
        echo '</div>
            <div class = "col-sm-9">
                <h2><a href ="'.the_permalink().'" title = "'.the_title_attribute().';"> '.get_the_title().'</a></h2>   
                '.get_the_content('Continue reading &raquo;').'
            </div>
    </div>';
    endwhile;
else :
    echo '<p>no content found </p>';
endif;

?>

<?php get_footer(); ?>